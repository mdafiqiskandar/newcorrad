import axios from 'axios'
// import jwt from 'jsonwebtoken'

const origin = window.location.origin
const baseurl =
  'https://qpm16xmlic.execute-api.ap-southeast-1.amazonaws.com/auth'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
}

export const mutations = {
  SET_CURRENT_USER(state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
    setDefaultAuthHeaders(state)
  },
}

export const getters = {
  // Whether the user is currently logged in.
  loggedIn(state) {
    return !!state.currentUser
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn({ commit, dispatch, getters }, credentials) {
    if (getters.loggedIn) return dispatch('validate')

    const options = {
      withCredentials: true,
      Origin: origin,
    }

    return axios
      .post(
        baseurl + '/login',
        {
          username: credentials.username,
          password: credentials.password,
        },
        options
      )
      .then((response) => {
        const user = response.data
        if (user.auth) {
          commit('SET_CURRENT_USER', user)
        }
        return user
      })
  },

  // register the user
  register({ commit, dispatch, getters }, { username, email, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post(baseurl + 'register', {
        username,
        email,
        password,
      })
      .then((response) => {
        const user = response.data
        return user
      })
  },

  validate({ commit, state }) {
    if (!state.currentUser) return Promise.resolve(null)

    return axios
      .get(baseurl + '/validate', {
        withCredentials: true,
        Origin: origin,
      })
      .then((response) => {
        const user = response.data
        commit('SET_CURRENT_USER', user)
        return user
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          commit('SET_CURRENT_USER', null)
        }
        return null
      })
  },

  // // Logs out the current user.
  logOut({ commit, dispatch, getters }) {
    return axios
      .get(baseurl + '/logout', {
        withCredentials: true,
        Origin: origin,
      })
      .then((response) => {
        commit('SET_CURRENT_USER', null)
      })
      .catch((error) => {
        console.log(error.response)
      })
  },
}

// // register the user
// resetPassword({ commit, dispatch, getters }, { email } = {}) {
//   if (getters.loggedIn) return dispatch('validate')

//   return axios.post('/api/reset', { email }).then((response) => {
//     const message = response.data
//     return message
//   })
// },

// ===
// Private helpers
// ===

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState(key, state) {
  if (state != null) {
    window.localStorage.setItem(
      key,
      JSON.stringify({
        username: state['username'],
        auth: state['auth'],
        admin: state['admin'],
      })
    )
    return
  }
  window.localStorage.setItem(key, state)
}

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.currentUser
    ? 'Bearer ' + state.currentUser.accessToken
    : ''
}
