import axios from 'axios'

const baseurl =
  'https://1sseyqo9qc.execute-api.ap-southeast-1.amazonaws.com/frame'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
}

export const actions = {
  getRoute({ commit, state }) {
    // const userdata = state.currentUser

    return axios
      .get(baseurl + '/menu')
      .then((response) => {
        // const user = response.data
        return response.data.allMenu
      })
      .catch((error) => {
        console.log(error.response)
      })
  },
}

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}
