import store from '@state/store'

// auth related routes
const authRoutes = [
  {
    path: '/login',
    name: 'login',
    component: () => lazyLoadView(import('@views/pages/account/login')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({
            name: 'dashboard',
          })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/register',
    name: 'register',
    component: () => lazyLoadView(import('@views/pages/account/register')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({
            name: 'dashboard',
          })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/confirm-account',
    name: 'confirm-account',
    component: () => lazyLoadView(import('@views/pages/account/confirm')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({
            name: 'dashboard',
          })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/forget-password',
    name: 'forget-password',
    component: () =>
      lazyLoadView(import('@views/pages/account/forgetPassword')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({
            name: 'dashboard',
          })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {
      authRequired: true,
      async beforeResolve(routeTo, routeFrom, next) {
        await store.dispatch('auth/logOut')
        const authRequiredOnPreviousRoute = routeFrom.matched.some(
          (route) => route.meta.authRequired
        )
        // Navigate back to previous page, or home as a fallback
        next(
          authRequiredOnPreviousRoute
            ? {
                name: 'dashboard',
              }
            : {
                ...routeFrom,
              }
        )
      },
    },
  },
]

// error pages
const errorPagesRoutes = [
  {
    path: '/404',
    name: '404',
    component: require('@views/pages/secondary/error-404').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  {
    path: '/500',
    name: '500',
    component: require('@views/pages/secondary/error-500').default,
    props: true,
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    redirect: '404',
  },
]

// dashboard
const dashboardRoutes = [
  {
    path: '/',
    name: 'Dashboard',
    header: '',
    icon: 'home',
    badge: {
      text: '',
      varient: '',
    },
    component: () => lazyLoadView(import('@views/pages/dashboard/dashboard')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({
      user: store.state.auth.currentUser || {},
    }),
  },
]

const uiKitchenSinkRoutes = [
  {
    path: '/ui',
    name: 'Kitchen Sink',
    header: 'UI/UX',
    icon: 'book',
    component: {
      render(c) {
        return c('router-view')
      },
    },
    meta: { authRequired: true },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
    children: [
      // ------------------------ CALENDAR MENU ---------------------------- //
      {
        path: 'calendar',
        name: 'Calendar',
        meta: { authRequired: true },
        component: () => lazyLoadView(import('@views/pages/apps/email/inbox')),
      },

      // ------------------------ EMAIL MENU ---------------------------- //
      {
        path: 'email',
        name: 'Email',
        icon: 'inbox',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        props: (route) => ({ user: store.state.auth.currentUser || {} }),
        children: [
          {
            path: 'inbox',
            name: 'Inbox',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/email/inbox')),
          },
          {
            path: 'read',
            name: 'Read Email',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/email/reademail')),
          },
          {
            path: 'compose',
            name: 'Compose Email',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/email/emailcompose')),
          },
        ],
      },

      // ------------------------ PROJECT MENU ---------------------------- //
      {
        path: 'apps',
        name: 'Project',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        props: (route) => ({ user: store.state.auth.currentUser || {} }),
        children: [
          {
            path: 'list',
            name: 'List',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/project/list')),
          },
          {
            path: 'detail',
            name: 'Detail',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/project/detail')),
          },
        ],
      },
      // ------------------------ TASK MENU ---------------------------- //
      {
        path: 'task',
        name: 'Task',
        icon: 'bookmark',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        props: (route) => ({ user: store.state.auth.currentUser || {} }),
        children: [
          {
            path: 'list',
            name: 'Task List',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/tasks/task-list')),
          },
          {
            path: 'task-board',
            name: 'Kanban Board',
            meta: { authRequired: true },
            component: () =>
              lazyLoadView(import('@views/pages/apps/tasks/task-board')),
          },
        ],
      },

      // ------------------------ PAGES MENU ---------------------------- //
      {
        path: 'pages',
        name: 'Pages',
        icon: 'file-text',
        header: 'Custom',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        children: [
          {
            path: 'starter',
            name: 'Starter',
            component: () =>
              lazyLoadView(import('@views/pages/secondary/starter')),
          },
          {
            path: 'invoice',
            name: 'Invoice',
            component: () =>
              lazyLoadView(import('@views/pages/secondary/invoice')),
          },
          {
            path: 'profile',
            name: 'Profile',
            component: () =>
              lazyLoadView(import('@views/pages/secondary/profile/')),
          },
          {
            path: 'activity',
            name: 'Activity',
            component: () =>
              lazyLoadView(import('@views/pages/secondary/activity')),
          },
          {
            path: 'pricing',
            name: 'Pricing',
            component: () =>
              lazyLoadView(import('@views/pages/secondary/pricing')),
          },
        ],
      },

      // ------------------------ UI ELEMENT MENU ---------------------------- //
      {
        path: 'element',
        name: 'UI Elements',
        icon: 'package',
        header: 'Components',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        children: [
          {
            path: 'bootstrap',
            name: 'Bootstrap UI',
            component: () => lazyLoadView(import('@views/pages/ui/bootstrap/')),
          },
          {
            path: 'widgets',
            name: 'Widgets',
            component: () => lazyLoadView(import('@views/pages/ui/widget/')),
          },
        ],
      },
      
      // ------------------------ ICONS MENU ---------------------------- //
      {
        path: 'icons',
        name: 'Icons',
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        children: [
          {
            path: 'feather',
            name: 'Feather',
            component: () =>
              lazyLoadView(import('@views/pages/ui/icons/feather')),
          },
          {
            path: 'unicons',
            name: 'Unicons',
            component: () =>
              lazyLoadView(import('@views/pages/ui/icons/unicons')),
          },
        ],
      },

      // ------------------------ FORMS MENU ---------------------------- //
      {
        path: 'forms',
        name: 'Forms',
        icon: 'file-text',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        children: [
          {
            path: 'basic',
            name: 'Basic',
            component: () => lazyLoadView(import('@views/pages/ui/forms/basic')),
          },
          {
            path: 'advanced',
            name: 'Advanced',
            component: () => lazyLoadView(import('@views/pages/ui/forms/advanced')),
          },
          {
            path: 'validation',
            name: 'Validation',
            component: () =>
              lazyLoadView(import('@views/pages/ui/forms/validation')),
          },
          {
            path: 'wizard',
            name: 'Wizard',
            component: () => lazyLoadView(import('@views/pages/ui/forms/wizard/')),
          },
          {
            path: 'editor',
            name: 'Editor',
            component: () => lazyLoadView(import('@views/pages/ui/forms/editor')),
          },
          {
            path: 'uploads',
            name: 'File Uploads',
            component: () => lazyLoadView(import('@views/pages/ui/forms/uploads')),
          },
        ],
      },

      // ------------------------ TABLES MENU ---------------------------- //
      {
        path: 'tables',
        name: 'Tables',
        icon: 'grid',
        meta: { authRequired: true },
        // create a container component
        component: {
          render(c) {
            return c('router-view')
          },
        },
        children: [
          {
            path: 'basic',
            name: 'Basic Tables',
            component: () =>
              lazyLoadView(import('@views/pages/ui/tables/basic-table')),
          },
          {
            path: 'advanced',
            name: 'Advanced Tables',
            component: () =>
              lazyLoadView(import('@views/pages/ui/tables/advanced-table')),
          },
        ],
      },
      
      // ------------------------ TABLES MENU ---------------------------- //
      {
        path: 'charts',
        name: 'Charts',
        icon: 'pie-chart',
        component: () => lazyLoadView(import('@views/pages/ui/chart/')),
        meta: { authRequired: true },
        props: (route) => ({ user: store.state.auth.currentUser || {} }),
      },
    ],
  },
]

var authProtectedRoutes = [...dashboardRoutes, ...uiKitchenSinkRoutes]
// console.log('before push protected')
// console.log(authProtectedRoutes)
// const menuArr = store.dispatch('route/getRoute')
// menuArr.then(function(result) {
//   console.log(result)

//   for (let i = 0; i < result.Count; i++) {
//     console.log(result.Items[0])
//     const arrMenu = []
//     var comp = null
//     var meta = null
//     var props = null

//     result.Items[i].route.component
//       ? (comp = [
//           {
//             component: () =>
//               lazyLoadView(
//                 import(`@views/pages/${result.Items[i].route.component}`)
//               ),
//           },
//         ])
//       : (comp = [
//           {
//             component: {
//               render(c) {
//                 return c('router-view')
//               },
//             },
//           },
//         ])

//     if (result.Items[i].route.meta) meta = [{ meta: { authRequired: true } }]

//     if (result.Items[i].route.props)
//       props = [
//         {
//           prop: (route) => ({ user: store.state.auth.currentUser || {} }),
//         },
//       ]

//     arrMenu.push({
//       path: result.Items[i].route.path,
//       name: result.Items[i].route.name,
//       header: result.Items[i].route.header,
//       icon: result.Items[i].route.icon,
//       badge: {
//         text: result.Items[i].route.badge.text,
//         varient: result.Items[i].route.badge.varient,
//       },
//       component: comp[0].component,
//       meta: meta[0].meta,
//       props: props[0].prop,
//     })

//     console.log('array menu')
//     console.log(arrMenu)

//     authProtectedRoutes.push(arrMenu[0])
//   }
//   console.log('route protected')
//   console.log(authProtectedRoutes)
// })

const allRoutes = [...authRoutes, ...authProtectedRoutes, ...errorPagesRoutes]

export { allRoutes, authProtectedRoutes }

// Lazy-loads view components, but with better UX. A loading view
// will be used if the component takes a while to load, falling
// back to a timeout view in case the page fails to load. You can
// use this component to lazy-load a route with:
//
// component: () => lazyLoadView(import('@views/my-view'))
//
// NOTE: Components loaded with this strategy DO NOT have access
// to in-component guards, such as beforeRouteEnter,
// beforeRouteUpdate, and beforeRouteLeave. You must either use
// route-level guards instead or lazy-load the component directly:
//
// component: () => import('@views/my-view')
//
function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    // A component to use while the component is loading.
    loading: require('@components/_loading').default,
    // Delay before showing the loading component.
    // Default: 200 (milliseconds).
    delay: 400,
    // A fallback component in case the timeout is exceeded
    // when loading the component.
    // error: require('@views/_timeout').default,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000,
  })

  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children)
    },
  })
}
